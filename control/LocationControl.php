<?php

require_once('../control/AccessControl.php');
require_once('../model/SerialNumberModel.php');
require_once('../model/Base32Model.php');
require_once('../model/UIDLocationModel.php');

require_once('../view/RedirectView.php');
require_once('../view/ErrorView.php');

class LocationControl extends AccessControl
{
  public function __construct($action){
    parent::__construct($action);
    $this->addAccess('move', 'Object Move');
  }

  public function render(){
    switch ($this->action){
    default:
      $widget = new ErrorView("Unsupported action '$this->action'");
      $widget->render();
      break;
    case 'move':
      $destination = null;
      $object = null;
      
      if (array_key_exists('location_uid_string', $_POST)){
        $destination = $_POST['location_uid_string'];
      }
      if (array_key_exists('obj_to_move_uid_string', $_POST)){
        $object = $_POST['obj_to_move_uid_string'];
      }

      if (is_null($destination) || is_null($object)){
        $widget = new ErrorView("Destination or object was not specified for move");
        $widget->render();
        break;
      }

      $destination = Base32Model::fromString($destination);
      $object = Base32Model::fromString($object);

      $destination = SerialNumberModel::findById($destination);
      if (is_null($destination)){
        $widget = new ErrorView("Destination needs to be a serial number");
        $widget->render();
        break;
      }
      /*
      if (!($destination instanceof SerialNumberModel)){
        $widget = new ErrorView("Wrong class ".get_class($destination));
        $widget->render();
        print_r($destination);
        break;
      }
      */

      $object = UIDModel::findById($object);
      if (is_null($object)){
        $widget = new ErrorView("Unable to find the object to move");
        $widget->render();
        break;
      }

      UIDLocationModel::updateUIDLocation($object, $destination);
      $base32Destination = Base32Model::fromInt($destination->getId());
      $widget = new RedirectView('/?group=uid&action=prepare_move&uid='.$base32Destination);
      $widget->render();
      break;
    }
  }
  
}
?>



  