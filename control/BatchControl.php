<?php

require_once('../control/AccessControl.php');
require_once('../model/BatchModel.php');
require_once('../model/UserModel.php');
require_once('../model/Base32Model.php');

require_once('../view/RedirectView.php');
require_once('../view/ErrorView.php');

class BatchControl extends AccessControl
{
  public function __construct($action){
    parent::__construct($action);
    $this->addAccess('create', 'Batch Create');
  }
  
  public function render(){
    switch ($this->action){
    default:
      $widget = new ErrorView("Unsupported action '$this->action'");
      $widget->render();
      break;
    case 'create':
      $uid = BatchModel::createNew(UserModel::getInstance(), null);
      $uid = Base32Model::fromInt($uid);
      $widget = new RedirectView('/?group=uid&action=search&search='.$uid, 2);
      $widget->render();
    }
  }
}