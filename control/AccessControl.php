<?php

require_once('../view/ErrorView.php');

/**
 * @file   AccessControl.php
 * @author Visti Andresen <talpa@talpa.dk>
 * @date   Mon Apr  6 09:00:05 2015
 * 
 * @brief  A helper class for implementing access control 
 * Inherit this class in all control classes
 * 
 */
class AccessControl
{
  private $actionList;
  protected $action;
  private $defaultAccess;

  /** 
   * Constructor 
   * 
   * @param action string, current action
   * @param defaultAccess string, the access level required if not specified by addAccess
   * 
   * @return 
   */
  protected function __construct($action, $defaultAccess="Site Access"){
    $this->action = $action;
    $this->defaultAccess = $defaultAccess;
    $this->actionList = array();
  }

  /** 
   * Add a different access level for a action than the default
   * 
   * @param action string, the action to change access level for
   * @param accessRequired string or null, the new access required null=can be accessed without previleges 
   * @param deniedMessage string or null, the message to display in case of access violations, null for the default message.
   * 
   */
  protected function addAccess($action, $accessRequired=null, $deniedMessage=null){
    $this->actionList[$action]=array($accessRequired, $deniedMessage);
  }

  /** 
   * Returns true if the current user has permission to use the current action
   *
   * @return bool
   */
  public function accessIsGranded(){
    $accessRequired = $this->defaultAccess;
    if (array_key_exists($this->action, $this->actionList)){
      $accessRequired = $this->actionList[$this->action][0];
    }

    if (is_null($accessRequired)){
      return true;
    }
    $user = UserModel::getInstance();
    return $user->hasAccess($accessRequired);    
  }

  /** 
   * Renders the denied message for the current action
   */
  public function renderAccessDenied(){
    $deniedMessage = "Access Denied";
    if (array_key_exists($this->action, $this->actionList)){
      if ($this->actionList[$this->action][1] != null){
        $deniedMessage  = $this->actionList[$this->action][1];
      }
    }
    $widget = new ErrorView($deniedMessage);
    $widget->render();
  }

  public function renderUnsupportedAction(){
    $widget = new ErrorView("Unsupported action '$this->action'");
    $widget->render();
  }
  
}

?>
