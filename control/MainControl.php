<?php

require_once('../view/ErrorView.php');
require_once('../view/JSView.php');

class MainControl {

  private static $instance = null;
  
	private $handler = null;
	private $group = "";
	private $action = "";
  private $standalone = false;

  public static function getInstance(){
    if (is_null(self::$instance)){
      self::$instance = new MainControl();
    }
    return self::$instance;
  }
  
	private function __construct(){
		if (array_key_exists('group', $_GET)){
			$this->group = htmlentities($_GET['group']);
		}
		if (array_key_exists('action', $_GET)){
			$this->action = htmlentities($_GET['action']);
		}

 		switch ($this->group){
    case 'user':
      require_once('../control/UserControl.php');
      $this->handler = new UserControl($this->action);
      break;
    case 'serial_number':
      require_once('../control/SerialNumberControl.php');
      $this->handler = new SerialNumberControl($this->action);
      break;
    case 'batch':
      require_once('../control/BatchControl.php');
      $this->handler = new BatchControl($this->action);
      break;
    case 'uid':
      require_once('../control/UIDControl.php');
      $this->handler = new UIDControl($this->action);
      break;
    case 'file':
      require_once('../control/FileControl.php');
      $this->handler = new FileControl($this->action);
      break;
    case 'location':
      require_once('../control/LocationControl.php');
      $this->handler = new LocationControl($this->action);
      break;
    case 'custom':
      require_once('../control/CustomControl.php');
      $this->handler = new CustomControl($this->action);
      break;
    case '':
      require_once('../control/FrontPageControl.php');
      $this->handler = new FrontPageControl($this->action);
      break;
		}
	}
	
	public function render(){
		if (is_object($this->handler)){
      if ($this->handler->accessIsGranded()){
        $this->handler->render();
      }
      else {
        $this->handler->renderAccessDenied();
      }
		}
		else {
			$widget = new ErrorView("Unsupported group '$this->group'");
			$widget->render();
		}
	}

  public function setStandalone(){
    $this->standalone = true;
  }

  public function isStandalone(){
    return $this->standalone;
  }
  
};

?>
