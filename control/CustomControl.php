<?php

require_once('../control/AccessControl.php');
require_once('../view/ErrorView.php');

class CustomControl extends AccessControl
{
  public function __construct($action){
    parent::__construct($action);
    $this->addAccess($action, 'Custom Actions');


    $this->handler = null;
    if (preg_match('/^[A-Za-z0-9]+$/', $action)){
      $controlFile = '../custom/control/'.$action.'Control.php';
      $this->subAction = "";
      if (array_key_exists('sub_action', $_GET)){
        $this->subAction = htmlentities($_GET['sub_action']);
      }              
      if (file_exists($controlFile)){
        require_once($controlFile);
        $this->handler = new ActionControl($this->subAction);
      }    
    }
  }

  public function render(){
    if (is_object($this->handler)){
      if ($this->handler->accessIsGranded()){
        $this->handler->render();
      }
      else {
        $this->handler->renderAccessDenied();
      }
		}
		else {
			$widget = new ErrorView("Unsupported custom action '$this->action'");
			$widget->render();
		}
  }
    
}

?>