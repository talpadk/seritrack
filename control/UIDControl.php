<?php

require_once('../control/AccessControl.php');
require_once('../model/UIDModel.php');
require_once('../model/Base32Model.php');
require_once('../model/LogEntryModel.php');

require_once('../view/UIDViewView.php');
require_once('../view/TableView.php');
require_once('../view/UIDLinkView.php');
require_once('../view/UIDPrepareMoveView.php');

class UIDControl extends AccessControl
{
  public function __construct($action){
    parent::__construct($action);
    $this->addAccess('view', 'UID View');
    $this->addAccess('prepare_move', 'UID View');
    $this->addAccess('add_log_entry', 'UID Log Add');
  }

  public function render(){
    switch ($this->action){
    default:
      $widget = new ErrorView("Unsupported action '$this->action'");
      $widget->render();
      break;
    case 'view':
      $uid = null;
      if (array_key_exists('uid', $_GET)){
        $uid = Base32Model::fromString($_GET['uid']);
      }
      if ($uid===null){
        $widget = new ErrorView("Unable to decode UID"); 
      }
      $uid = UIDModel::findById($uid);
      if ($uid === null){
        $widget = new ErrorView("Unable to find the requested UID in the database");
      }
      else {
        $widget = new UIDViewView($uid);
      }
      
      $widget->render();
      break;
    case 'search':
      ///@ToDo refactor to a propper view
      $search = null;
      $widget = new ErrorView('No search results found');
      if (array_key_exists('search', $_GET)){
        $search = $_GET['search'];
        $uid = null;
        $uid = Base32Model::fromString($search);
        if (!($uid===null)){ $uidModel = UIDModel::findById($uid); }
        if (is_object($uidModel)){
          $widget = new UIDViewView($uidModel);
        }
        else {
          $logEntries = LogEntryModel::findByMessage($search);
          if (count($logEntries)>0){
            $header = array ('UID'=>'uid',
                             'Message'=>'message');
            $data = array();
            foreach ($logEntries as $logEntry){
              $row = array();
              $uid = $logEntry->getUid();
              $uid = new UIDLinkView($uid);
              $uid = $uid->renderToText();
              $message = $logEntry->getMessage();
              $row['uid']=$uid;
              $row['message']=$message;
              $data[]=$row;
            }
            $widget = new TableView($header, $data);
          }
        }
      }
      print '<div class="UIDSearchResults">';
      $widget->render();
      print '</div>';
      break;

    case 'add_log_entry':
      $uid = null;
      $comment = null;

      if (array_key_exists('comment', $_POST)){
        $comment = $_POST['comment'];
      }
      if (array_key_exists('uid', $_POST)){
        $uid = UIDModel::findById((int)$_POST['uid']);
      }
      if ((!($uid===null)) and (!($uid===null))){
        LogEntryModel::add($uid->getId(), $comment);
        $widget = new UIDViewView($uid);
      }
      else {
        $widget = new ErrorView("Unable to find UID to add comment for");
      }
      $widget->render();
      break;

    case 'prepare_move':
      $uid = null;
      if (array_key_exists('uid', $_GET)){
        $uid = UIDModel::findById((int)Base32Model::fromString($_GET['uid']));
      }
      if (!($uid===null)){
        $widget = new UIDPrepareMoveView($uid);
      }
      else {
        $widget = new ErrorView("Unable to find the requested UID");
      }
      $widget->render();
      break;
    }
  }
  
}

?>
