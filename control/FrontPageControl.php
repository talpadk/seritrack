<?php

require_once('../control/AccessControl.php');
require_once('../view/FrontPageView.php');

class FrontPageControl extends AccessControl
{
  public function __construct($action){
    parent::__construct($action);
  }

  public function render(){
    $widget = new FrontPageView();
    $widget->render();

  }
  
}

?>
