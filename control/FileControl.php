<?php

require_once('../control/AccessControl.php');

require_once('../model/FileLocationsModel.php');
require_once('../model/Base32Model.php');
require_once('../model/UIDModel.php');

require_once('../view/DownloadView.php');
require_once('../view/RedirectView.php');

class FileControl extends AccessControl
{
  public function __construct($action){
    parent::__construct($action);
    $this->addAccess('upload_to_uid', 'UID File Upload');
  }

  public function render(){
    global $CONFIGURATION;
    switch ($this->action){
    default:
      $this->renderUnsupportedAction();
      break;
    case 'download':
      $file = realpath($_GET['file']);
      if (FileLocationsModel::pathIsValidForDownload($file)){
        $widget = new DownloadView($file);
      }
      else {
        $widget = new ErrorView("Not allowed to download from '$file'");
      }
      $widget->render();
      break;
    case 'upload':
      if (!array_key_exists('uid', $_POST)){
        $widget = new ErrorView('UID not specified for upload target<br /><div class="Note">The request may have exceeded post_max_size in php.ini</div>');
        $widget->render();
        break;
      }
      $uid = UIDModel::findById(Base32Model::fromString($_POST['uid']));
      if (is_object($uid)){
        if (!isset($_FILES['upload_file']['error'])){
          $widget = new ErrorView("Errors occured while uploading your file");
          $widget->render();
          break;
        }
        if (is_array($_FILES['upload_file']['error'])){
          $widget = new ErrorView("Upload of more than one file at a time is currently not supported");
          $widget->render();
          break;
        }
        switch($_FILES['upload_file']['error']){
        default:
          $widget = new ErrorView("Unspecified errors occured while uploading your file");
          $widget->render();
          break;
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
          $widget = new ErrorView("Maximum file size exeeded (php.ini)");
          $widget->render();
          break;
        case UPLOAD_ERR_OK:
          break;
        }
        if ($_FILES['upload_file']['error'] != UPLOAD_ERR_OK){
          break;
        }
        
        if ($_FILES['upload_file']['size'] > $CONFIGURATION['UPLOAD_MAX_SIZE']) {
          $widget = new ErrorView('File is too large for the current configuration settings');
          $widget->render();
          break;
        }

        if (strlen($_FILES['upload_file']['tmp_name'])==0){
          $widget = new ErrorView('Did not recieve any file');
          $widget->render();
          break;
        } 

        if (FileLocationsModel::isFileValidForUpload($_FILES['upload_file']['tmp_name'], $_FILES['upload_file']['name'])){
          $uidPath = FileLocationsModel::getUIDPath($uid);
          if (!file_exists($uidPath)){
            mkdir($uidPath, 0700, true);
          }
          $uidPath = realpath($uidPath);
          $fullPath = $uidPath.'/'.$_FILES['upload_file']['name'];
          if (strpos($_FILES['upload_file']['name'], '/')){
            $widget = new ErrorView("Filename not valid, it modifies path real ".realpath($fullPath).' alt '.$fullPath);
          }
          else {
            if (file_exists($fullPath)){
              $widget = new ErrorView("File already exists");
            }
            else {
              if (move_uploaded_file($_FILES['upload_file']['tmp_name'], $fullPath)){
                $widget = new RedirectView('/?group=uid&action=view&uid='.Base32Model::fromInt($uid->getId()));
              }
              else {
                $widget = new ErrorView("Failed to store the file on the server");
              }
            }
          }
        }
        else {
          $widget = new ErrorView("File content type not supported or does not match the filename");
        }
      }
      else {
        $widget = new ErrorView("Unable to find the UID to upload data to");
      }
      $widget->render();    
      break;
    }
  }
  
}

?>