<?php

require_once('../control/AccessControl.php');
require_once('../model/UserModel.php');

require_once('../view/ErrorView.php');
require_once('../view/MessageBoxView.php');
require_once('../view/UserSignupFormView.php');
require_once('../view/RedirectView.php');
  
class UserControl extends AccessControl
{
	public function __construct($action) {	
		parent::__construct($action);
    $this->addAccess('logout');
    $this->addAccess('login');
    $this->addAccess('signup', 'User Signup');
    $this->addAccess('do_signup', 'User Signup');
	}

	public function render(){
		switch ($this->action){
    default:
      $widget = new ErrorView("Unsupported action '$this->action'");
      $widget->render();
      break;
    case 'login':
      $email = null;
      $password = null;

      if (array_key_exists('email', $_POST)) { $email = $_POST['email']; }
      if (array_key_exists('password', $_POST)) { $password = $_POST['password']; }

      if (UserModel::login($email, $password)){
        $widget = new RedirectView('/', 1);
        $widget->render();
        $widget = new MessageBox("Login successful");
      }
      else {
        $widget = new ErrorView("Unable to login");
      }
      $widget->render();
      break;
    case 'logout':
      UserModel::logout();
      $widget = new MessageBox("You have been logged out");
      $widget->render();
      break;
    case 'signup':
      $widget = new UserSignupFormView();
      $widget->render();
      break;
    case 'do_signup':
      $email = null;
      $screenName = null;
      $password = null;
      $password2= null;
      
      if (array_key_exists('email', $_POST)) { $email = $_POST['email']; }
      if (array_key_exists('screen_name', $_POST)) { $screenName = $_POST['screen_name']; }
      if (array_key_exists('password', $_POST)) { $password = $_POST['password']; }
      if (array_key_exists('password2', $_POST)) { $password2 = $_POST['password2']; }

      if ((!is_null($email)) && (!is_null($password)) && (!is_null($password2)) && (!is_null($screenName))){
        if ($password != $password2){
          $widget = new ErrorView("Passwords didn't match, please enable javascript in your browser.");
        }
        else {
          ///@todo add strength constraints on the password
          $errorMessage = UserModel::addUser($email, $screenName, $password);
          if (is_null($errorMessage)){
            $widget = new MessageBox("Signup successful");
          }
          else {
            $widget = new ErrorView("Signup failed<br /><br />$errorMessage");                    
          }
        }
      }
      else {
        $widget = new ErrorView("Somehow some form fields were missing.");
      }
      $widget->render();
      break;
		}
	}
	
}

?>
