# Seritrack
## About 
Seritrack is a php and MySQL based serial and batch number tracker with features including:

* Unique ID across all ID types.<br />Eq. you don't need to know if a number is a Serial Number or a Batch Number to look it up.
* Handling of locations.<br/>It is possible to track location information by putting objects inside a Serial Number tree
* Text logs for UIDs
* Upload of files to an UIDs
* User permissions

## Configuring seritrack
Seritrack is configured by placing a **config.php** file in the root of the repository.   
The configuration among other things configures the mySQL username and password.

Use **config_template.php** as a template for this.

## Database setup
The folder database_versions contains sql files for creating and updating the seritrack database tables.  
The sql assumes that no database table prefix is used in config.php

If the database name is changed or a table prefix is used the SQL can be piped though the script **utils/cli_patch_sql.php**
The script is intended to be run from the root of the repository and reads the config.php
>cat database_versions/0.sql | php utils/patchSql.php 

## User creation
As the default guest users are allowed to signup for a user account.  
You may want to disable this after an initial administrator account has been created.

The default role for new users can be configured in **config.php**  
At present there isn't a direct link for signing up but the signup page is located under
>http://your_server_name_here/?group=user&action=signup

## Roles and permissions
Currently there is no web interface for administrating users and there roles.  
The users roles and the permissions granted per role are controlled using the tables ***users*** and ***role_access_rights***

The users table has a column called roles, the roles are a comma separated list of roles names for the given user.

In the role_access_rights there is an row for each permission granted to a role name.  
The default is that the role "Guest" is grantet access to "User Signup"

The currently available access privilege strings are:

* 'User Signup'
* 'Site Access'
* 'SN Create'
* 'UID View'
* 'UID Log Add'
* 'Batch Create'
* 'UID File Upload'
* 'Object Move'

## File storage
Binary data uploaded to an UID isn't stored inside the database but is instead stored in the file system.

You must create two folders in the root of the repository for the web service.

* ***files/*** is used to store the actual data
* ***thumbs/*** is used to store auto regenerating preview thumbnail images of the data

The folders must be owned / writeable by **www-data** or whatever user is running the webserver.
