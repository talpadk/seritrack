<?php

$CONFIGURATION = array(
  #General options
  #If true the seritrack instance is considered a development instance which enables php debug output 
  'DEVELOPMENT'=>false,
  #The minimum of characters in a base 32 encoded UUID used when displaying it, the number is zero padded
  'BASE32_MIN_LENGTH'=>3,
  
  #(Optional) Use legacy paths for storing uploaded data
  #'USE_LEGACY_FILE_PATH' => false,
  #(Optional) The number of characters to use when building the file storage path,
  #   as this is hard to change later on (for legacy file paths) it is recommended to set it large enough.
  #   It does however come at a cost of additional levels of directories.
  #   Defaults to BASE32_MIN_LENGTH
  #'BASE32_PATH_MIN_LENGTH'=>3,
  
  #File upload file size limit
  'UPLOAD_MAX_SIZE' => 100*1024*1024,
  #(Optional) The roles given to new users, if not specified the new user has no roles
  'DEFAULT_ROLES' => ''

  #MySQL configuration
  #The host name / IP address of the MySQL database server
  'DATABASE_HOST'=>'127.0.0.1',
  #The username used when accessing the MySQL db
  'DATABASE_USER'=>'seritrack',
  #The password for the MySQL user
  'DATABASE_PASSWORD'=>'',
  #The name of the database
  'DATABASE_DATABASE'=>'seritrack',
  #(Optional) if set all tables names are prefixed with this string
  'DATABASE_TABLE_PREFIX'=>'',

  #Parameters for ssl_set, if you only want to use a server cert simply set SQL_SSL_CA to the full path of the certificate
  'SQL_SSL_KEY'=>NULL,
  'SQL_SSL_CERT'=>NULL,
  'SQL_SSL_CA'=>NULL,
  'SQL_SSL_CA_PATH'=>NULL,
  'SQL_SSL_CIPHER'=>NULL,

  #If not NULL MYSQLI_OPT_SSL_VERIFY_SERVER_CERT will be set to this value
  'SQL_SSL_VERIFY_SERVER_CERT'=>NULL,
);
?>
