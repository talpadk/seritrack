-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2015 at 04:19 PM
-- Server version: 5.5.46-0+deb8u1
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seritrack`
--
CREATE DATABASE IF NOT EXISTS `seritrack` DEFAULT CHARACTER SET utf8 COLLATE utf8_danish_ci;
USE `seritrack`;

-- --------------------------------------------------------

--
-- Table structure for table `log_entries`
--

CREATE TABLE IF NOT EXISTS `log_entries` (
  `uid` bigint(20) unsigned NOT NULL,
  `message` varchar(255) COLLATE utf8_danish_ci NOT NULL,
  `created` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_access_rights`
--

CREATE TABLE IF NOT EXISTS `role_access_rights` (
`id` int(10) unsigned NOT NULL,
  `role` varchar(255) COLLATE utf8_danish_ci DEFAULT NULL,
  `access` varchar(255) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `uids`
--

CREATE TABLE IF NOT EXISTS `uids` (
`id` bigint(20) unsigned NOT NULL,
  `type` smallint(5) unsigned NOT NULL,
  `created_by_user_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_danish_ci NOT NULL,
  `screen_name` varchar(10) COLLATE utf8_danish_ci NOT NULL,
  `password` varchar(120) COLLATE utf8_danish_ci NOT NULL,
  `roles` text COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_entries`
--
ALTER TABLE `log_entries`
 ADD KEY `uid` (`uid`);

--
-- Indexes for table `role_access_rights`
--
ALTER TABLE `role_access_rights`
 ADD PRIMARY KEY (`id`), ADD KEY `role` (`role`);

--
-- Indexes for table `uids`
--
ALTER TABLE `uids`
 ADD PRIMARY KEY (`id`), ADD KEY `type` (`type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `role_access_rights`
--
ALTER TABLE `role_access_rights`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uids`
--
ALTER TABLE `uids`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
