USE `seritrack`;

INSERT INTO `users` (`email`, `screen_name`, `password`, `roles`) VALUES
('guest', 'Guest', '', 'Guest');

INSERT INTO `role_access_rights` (`role`, `access`) VALUES
('Guest', 'User Signup');
