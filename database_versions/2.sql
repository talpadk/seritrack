USE `seritrack`;

CREATE TABLE IF NOT EXISTS `locations` (
  `parent` bigint(20) NOT NULL,
  `child` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `removed` datetime DEFAULT NULL,
  `created_by_user_id` int(11) NOT NULL,
  `removed_by_user_id` int(11) DEFAULT NULL,
  KEY `parent` (`parent`,`child`,`removed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
