<?php
ob_start();
session_start();

require_once('../config.php');

global $CONFIGURATION;
$siteTitle = "Seritrack";
if ($CONFIGURATION['DEVELOPMENT']){
	error_reporting(E_ALL | E_STRICT);
  if (ini_set('display_errors', '1')===false){
    die("Development: Failed to set display_errors");
  }
  $siteTitle .= " - DEVELOPMENT VERSION";
}



require_once('../view/LoginView.php');
require_once('../view/LogoutView.php');
require_once('../control/MainControl.php');
require_once('../view/JSView.php');
require_once('../model/UserModel.php');

$control = MainControl::getInstance();
ob_clean();
ob_start();
$control->render();
$content = ob_get_contents();
ob_end_clean();

if (!$control->isStandalone()){
  print '<html>';
  print '<head>';
  print "<title>$siteTitle</title>";
  print '<link href="css/site.css" rel="stylesheet" type="text/css">';
  print '<link href="css/uid_log_view.css" rel="stylesheet" type="text/css">';
  print '<link href="css/uid_files_view.css" rel="stylesheet" type="text/css">';
  print '<link href="css/uid_control.css" rel="stylesheet" type="text/css">';
  
  JSView::render();

  print '</head>';
  print '<body>';

  print '<div class="Page">';
  print '<div class="Header">';


  if (UserModel::isLoggedOn()){
    $widget=new LogoutView();
  }
  else {
    $widget=new LoginView();
  }
  $widget->render();

  print '<a href="/"><img style="float:left;" src="images/logo.png"></a>';
  print '</div>';
  
  print '<div class="PageContent">';
}

print $content;
  
if (!$control->isStandalone()){
  print '</div>';
  
  print '</div>';
  print '</body>';
  print '</html>';
}
?>