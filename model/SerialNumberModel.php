<?php

require_once('../model/UIDModel.php');
require_once('../model/UIDTypeModel.php');
  
class SerialNumberModel extends UIDModel
{
  public static function createNew($user, $parentSN){
    return parent::createNewUID($user, UIDTypeModel::findTypeByName('SerialNumber'));
  }

  public static function findById($id){
    $object = parent::findById($id);
    if (!is_null($object)){
      if ($object->getTypeId() != UIDTypeModel::findTypeByName('SerialNumber')){
        $object = null;
      }
    }
    return $object;
  }
}

?>
