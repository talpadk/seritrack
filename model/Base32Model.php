<?php

require_once('../config.php');


class Base32Model
{
  private static $encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
  

  private function __construct(){
    
  }

  /** 
   * Converts an integer to a base32 text representation
   * 
   * @param value, int the value to convert 
   * @param minLength, int/null the minimum length of the output if null it uses $CONFIGURATION['BASE32_MIN_LENGTH'] as the value. 
   * 
   * @return string
   */
  public static function fromInt($value, $minLength=null) : string {
    global $CONFIGURATION;
    $result = "";
    $value = (int)$value;
    if (!is_null($minLength)){
      $minLength = (int)$minLength;
    }
    else {
      $minLength = $CONFIGURATION['BASE32_MIN_LENGTH'];
    }
    for ($i=0; $i<$minLength; $i++){
      $result = substr(self::$encoding, $value%32, 1).$result;
      $value = (int)($value/32);
    }
    while ($value > 0){
      $result = substr(self::$encoding, $value%32, 1).$result;
      $value = (int)($value/32);
    }
    return $result;
  }

  /** 
   * Attempts to convert a base32 text string to an integer
   * 
   * @param value string, the base32 string to convert.
   * 
   * @return int or null if an illegal symbol is found.
   */
  public static function fromString($value) : ?int {
    $result = 0;
    $value = strtoupper($value);
    $length = strlen($value);
    for ($i=0; $i<$length; $i++){
      $pos = strpos(self::$encoding, $value[$i]);
      if ($pos===false){
        return null;
      }
      else {
        $result = $result*32+$pos;
      }
    }
    return $result;
  }

  
}

?>
