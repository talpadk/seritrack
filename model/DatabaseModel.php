<?php

require_once('../config.php');

class DatabaseModel
{
	private $databaseConnection;
	
	private function __construct($host, $user, $password, $database){
		global $CONFIGURATION;
		///@todo implement error handling 
		$this->databaseConnection = new mysqli();
        
        if (array_key_exists('SQL_SSL_VERIFY_SERVER_CERT', $CONFIGURATION) && !is_null($CONFIGURATION['SQL_SSL_VERIFY_SERVER_CERT'])){
            $this->databaseConnection->options(MYSQLI_OPT_SSL_VERIFY_SERVER_CERT, $CONFIGURATION['SQL_SSL_VERIFY_SERVER_CERT']);
        }
        $key = NULL;
        $cert = NULL;
        $ca = NULL;
        $caPath = NULL;
        $cipher = NULL;
        if (array_key_exists('SQL_SSL_KEY', $CONFIGURATION)){
            $key = $CONFIGURATION['SQL_SSL_KEY'];
        }
        if (array_key_exists('SQL_SSL_CERT', $CONFIGURATION)){
            $cert = $CONFIGURATION['SQL_SSL_CERT'];
        }
        if (array_key_exists('SQL_SSL_CA', $CONFIGURATION)){
            $ca = $CONFIGURATION['SQL_SSL_CA'];
        }
        if (array_key_exists('SQL_SSL_CA_PATH', $CONFIGURATION)){
            $caPath = $CONFIGURATION['SQL_SSL_CA_PATH'];
        }
        if (array_key_exists('SQL_SSL_CIPHER', $CONFIGURATION)){
            $cipher = $CONFIGURATION['SQL_SSL_CIPHER'];
        }
        
		$this->databaseConnection->ssl_set($key, $cert, $ca, $caPath, $cipher);
		$this->databaseConnection->real_connect($host, $user, $password, $database);

	}

  /** 
   * Returns an instance of the database
   * 
   * @return DatabaseModel
   */
	public static function getInstance(){
		global $CONFIGURATION;
		static $instance = null;
		if (is_null($instance)){
			$instance = new DatabaseModel($CONFIGURATION['DATABASE_HOST'],
												$CONFIGURATION['DATABASE_USER'],
												$CONFIGURATION['DATABASE_PASSWORD'],
												$CONFIGURATION['DATABASE_DATABASE']);
		}
		return $instance;
	}

  public function escape($data){
    return $this->databaseConnection->real_escape_string($data);
  }

  public function executeSql($sql){
    return $this->databaseConnection->query($sql);
  }

  public function getLastInsertId(){
    return $this->databaseConnection->insert_id;
  }

  public function isResultSuccess($result){
    return $result===true;
  }

  /** 
   * Returns the name of the table prefixed with the prefix if any is to be used.
   * 
   * @param name the "plain" name of the table
   * 
   * @return string
   */
  public function getTableNameWithPrefix($name){
    global $CONFIGURATION;
    $prefix = "";
    if (array_key_exists('DATABASE_TABLE_PREFIX', $CONFIGURATION)){
      $prefix = $CONFIGURATION['DATABASE_TABLE_PREFIX'];
    }
    return $this->escape($prefix.$name);
  }
};

?>
