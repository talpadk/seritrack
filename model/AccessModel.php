<?php

require_once ('../model/DatabaseModel.php');

class AccessModel
{
  private function __construct(){
    
  }

  /** 
   * Finds all access rights for a set of roles
   * 
   * @param roles array of string, the set of named roles
   * 
   * @return associative array of ('AccessName'=>void, ...)
   */
  public static function fetchAccessesForRoles($roles){
    $resultArray = array();
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('role_access_rights');
    
    if (!is_array($roles)){
      return $resultArray;
    }
    $escapedRoles = array();
    foreach ($roles as $role){
      $escapedRoles[]=$database->escape($role);
    }

    
    $roleString = "('".implode("','", $escapedRoles)."')";
    $sql = "SELECT access FROM $table WHERE role IN $roleString GROUP BY access";
    $result =  $database->executeSql($sql);
    if (is_object($result)){
      while($row = $result->fetch_row()){
        $resultArray[$row[0]]=1;
      }
      $result->free();
    }
    return $resultArray;
  }
}

?>
