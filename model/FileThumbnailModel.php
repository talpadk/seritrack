<?php

require_once('../model/FileLocationsModel.php');

class FileThumbnailModel
{
  public function __construct(){
    
  }

  public static function getWidth(){
    return 100;
  }

  public static function getHeight(){
    return 100;
  }
  
  public static function getThumbailAsHtmlImage($file){
    $html  = '<img src="/?group=file&action=download&file='.urlencode(FileThumbnailModel::getThumbnailPath($file)).'" ';
    $html .= 'width="'.FileThumbnailModel::getWidth().'" ';
    $html .= 'height="'.FileThumbnailModel::getHeight().'" ';
    $html .= " />";
    return $html;
  }
  
  public static function getThumbnailPath($file){
    $installPath = realpath(dirname(__FILE__).'/..');
    $filePath = realpath($file);
    $pos = strpos($filePath, $installPath);
    if ($pos !== false && $pos==0){
      $filePath = substr($filePath, strlen($installPath)+1);
      if (FileLocationsModel::pathIsValidForDownload('../'.$filePath)){
        $supportedTypes = array('image/jpeg'       =>1,
                                'image/png'        =>1,
                                'application/pdf'  =>1,
        );
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($finfo, '../'.$filePath);
        if (array_key_exists($mimeType, $supportedTypes)){
          $inputFile = '../'.$filePath;
          $outputFile = '../thumbs/'.$filePath.'.png';
          $outputPath = dirname($outputFile);
          if (file_exists($outputFile)){
            return $outputFile;
          }
          if (!file_exists($outputPath)){
            mkdir($outputPath, 0700, true);
          }
          $width = FileThumbnailModel::getWidth();
          $height = FileThumbnailModel::getHeight();
          set_time_limit(60);
          if ($mimeType=="application/pdf"){
            $inputFile .= '[0]';
          }
          $inputFile = escapeshellarg($inputFile);
          $outputEscFile = escapeshellarg($outputFile);
          $convertCommand = "convert $inputFile -scale ".$width."x$height -gravity center -background transparent -extent ".$width."x$height $outputEscFile";
          if (system($convertCommand)!==false){
            return $outputFile;
          }
        }
      }
    }
    return "../www/images/nopreview.png";
  }
}

?>
