<?php

require_once('../model/UserModel.php');
require_once('../model/DatabaseModel.php');


class LogEntryModel
{
  private $uid;

  private $message;

  private $created;

  private $userId;
  
  private static $COLS_FOR_CREATION = "uid, message, created, user_id";
  
  private function __construct($row){
    $this->uid = $row['uid'];
    $this->message = $row['message'];
    $this->created = $row['created'];
    $this->userId = $row['user_id'];
  }

  public static function add($uid, $comment){
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('log_entries');

    $uid = (int)$uid;
    $comment = $database->escape($comment);

    $user = UserModel::getInstance();
    $userId = (int)$user->getId();
    $now = date('Y-m-d H:i:s');

    $sql = "INSERT INTO $table (uid, message, created, user_id) VALUES ($uid, '$comment', '$now', $userId)";
    $database->executeSql($sql);
  }

  /** 
   * Returns an array of all the log messages for the given UID
   * 
   * @param uid int, the UID to find messages for
   * @param sortAscending bool, if true the messages will be returned sorted ascending with regard to creation date
   * 
   * @return array of LogEntryModel 
   */
  public static function findByUid($uid, $sortAscending=0){
    $uid = (int)$uid;
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('log_entries');
    $returnValue = array();
    
    if ($sortAscending){
      $order = "ORDER BY created ASC";
    }
    else {
      $order = "ORDER BY created DESC";
    }
    
    $sql = "SELECT ".self::$COLS_FOR_CREATION." FROM $table WHERE uid=$uid $order";
    $result = $database->executeSql($sql);
    if (is_object($result)){
      while (!(($row=$result->fetch_assoc())===null)){
        $returnValue[]=new LogEntryModel($row);
      }
    }
    return $returnValue;
  }

  /** 
   * Returns an array of all log messages containing a specific content
   * 
   * @param message string, the sub string the message must contain
   * 
   * @return array of LogEntryModel 
   */
  public static function findByMessage($message){
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('log_entries');
    $message = $database->escape($message);
    $returnValue = array();

    $sql = "SELECT ".self::$COLS_FOR_CREATION." FROM $table WHERE message LIKE '%$message%'";
    $result = $database->executeSql($sql);
    if (is_object($result)){
      while (!(($row=$result->fetch_assoc())===null)){
        $returnValue[]=new LogEntryModel($row);
      }
    }
    return $returnValue;  
  }

  /** 
   * The log entry text
   * 
   * 
   * @return string
   */
  function getMessage(){
    return $this->message;
  }

  /** 
   * Returns the User that created the log entry
   * 
   * @return UserModel or null
   */
  function getUser(){
    return UserModel::findById($this->userId);
  }

  /** 
   * The date time when the log entry was created
   * 
   * @return string
   */
  function getCreated(){
    return $this->created;
  }
  
  /** 
   * The Uid this log entry relates to
   * 
   * 
   * @return UIDModel or null
   */
  function getUid(){
    return UIDModel::findById($this->uid);
  }
  


  
}

?>
