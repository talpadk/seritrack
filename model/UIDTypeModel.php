<?php

class UIDTypeModel
{
  const SERIAL_NUMBER = 1;
  const BATCH = 2;

  
  public function __construct(){
    
  }

  public static function findTypeNameById($typeId){
    $typeId = (int)$typeId;
    switch ($typeId){
    default:
      return "Undefined";
    case self::SERIAL_NUMBER:
      return "SerialNumber";
    case self::BATCH:
      return "Batch";
    }
  }
  
  public static function findTypeByName($typeName){
    switch ($typeName){
    default:
      return null;
    case 'SerialNumber':
      return self::SERIAL_NUMBER;
    case 'Batch':
      return self::BATCH;
    }
  }
}

?>
