
<?php

/**
 * @file   FileLocationsModel.php
 * @author Visti Andresen <talpa@Hermes.talpa.dk>
 * @date   Sat Dec 12 20:10:29 2015
 * 
 * @brief  Handles different aspects of file locations, mime types and up-/down-load validity 
 * 
 * 
 */

class FileLocationsModel{


  /** 
   * Returns true if a user is allowed to view / download a given path.
   * 
   * @param path string, the path in question
   * 
   * @return bool, true if ok
   */
  public static function pathIsValidForDownload($path){
    $installPath = realpath(dirname(__FILE__).'/..');
    $path = realpath($path);

    $pos = strpos($path, "$installPath/files");
    if ($pos !== false && $pos==0){
      return true;
    }
    $pos = strpos($path, "$installPath/www/images");
    if ($pos !== false && $pos==0){
      return true;
    }
    $pos = strpos($path, "$installPath/thumbs");
    if ($pos !== false && $pos==0){
      return true;
    }

    return false;
  }

  public static function isFileValidForUpload($tempName, $filename){
    $supportedTypes = array('image/jpeg'       =>array('.jpg', '.jpeg'),
                            'image/png'        =>array('.png'),
                            'application/pdf'  =>array('.pdf'),
                            'application/zip'  =>array('.zip'),
                            'text/plain'       =>array('.txt'),
                            'message/rfc822'   =>array('.txt'),
                           );
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mimeType = finfo_file($finfo, $tempName);
    if (array_key_exists($mimeType, $supportedTypes)){
      $suffixesAllowed = $supportedTypes[$mimeType];
      foreach ($suffixesAllowed as $suffix){
        $suffixPos = strpos($filename, $suffix);
        if ($suffixPos === strlen($filename)-strlen($suffix)){
          return true;
        }
      }
    }
    return false;
  }
  
  public static function getUIDPath($uid){
    global $CONFIGURATION;
    $uidLength = null;
    if (array_key_exists('BASE32_PATH_MIN_LENGTH', $CONFIGURATION)){
      $uidLength = $CONFIGURATION['BASE32_PATH_MIN_LENGTH'];
    }
    $uidString = Base32Model::fromInt($uid->getId(), $uidLength);
    
    $path = '';
    $uidStringLength = strlen($uidString);

    if (array_key_exists('USE_LEGACY_FILE_PATH', $CONFIGURATION) &&
        $CONFIGURATION['USE_LEGACY_FILE_PATH'] === true){
      for ($i=$uidStringLength-1; $i>=0; $i--){
        $path .= substr($uidString, $i).'/';
      }
    }
    else {
      for ($i=0; $i<$uidStringLength; $i++){
        $path .= $uidString[$i].'/';
      }
    }
    return '../files/'.$path.'_files';
  }


}


?>