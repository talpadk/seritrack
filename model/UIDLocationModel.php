<?php

require_once ('../model/UserModel.php');
require_once ('../model/UIDModel.php');
require_once ('../model/DatabaseModel.php');

class UIDLocationModel
{


  public static function getLocationForUID(UIDModel $uid) : ?UIDModel {
    $id = (int)$uid->getId();
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('locations');
    
    $sql = "SELECT parent FROM $table WHERE child=".$id." AND removed IS NULL ORDER BY created DESC LIMIT 1";

    $result = $database->executeSql($sql);
    if (is_object($result) && $result->num_rows>0){
      $row = $result->fetch_assoc();
      $id = $row['parent'];
      return UIDModel::findById($id);
    }
    else {
      return null;
    }
  }

  public static function getContentForUID($uid){
    $id = (int)$uid->getId();
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('locations');
    
    $sql = "SELECT child FROM $table WHERE parent=".$id." AND removed IS NULL ORDER BY child ASC";

    $result = $database->executeSql($sql);
    if (is_object($result) && $result->num_rows>0){
      $objects = array();
      while ($row = $result->fetch_assoc()) {
        $id = $row['child'];
        $objects[] = UIDModel::findById($id);
      }
      return $objects;
    }
    else {
      return null;
    }
  }

  public static function removeUIDFromAllLocations($uid){
    $id = (int)$uid->getId();
    $user = UserModel::getInstance();

    if ($user->hasAccess("Object Move")){
      $userId = (int)$user->getId();
      $now = date('Y-m-d H:i:s');
      
      $database = DatabaseModel::getInstance();
      $table = $database->getTableNameWithPrefix('locations');
    
      $sql = "UPDATE $table SET removed='$now', removed_by_user_id=$userId WHERE child=$id AND removed IS NULL";
      $database->executeSql($sql);
    }
  }

  public static function updateUIDLocation($uid, $newLocation){
    $id = (int)$uid->getId();
    $user = UserModel::getInstance();

    if ($user->hasAccess("Object Move")){
      UIDLocationModel::removeUIDFromAllLocations($uid);
      
      $userId = (int)$user->getId();
      $now = date('Y-m-d H:i:s');
      $newLocationId = (int)$newLocation->getId();

      $database = DatabaseModel::getInstance();
      $table = $database->getTableNameWithPrefix('locations');
    
      $sql = "INSERT INTO $table (parent, child, created, created_by_user_id) VALUES ($newLocationId, $id, '$now', $userId)";
      $database->executeSql($sql);      
    }
  }
  
}


?>
