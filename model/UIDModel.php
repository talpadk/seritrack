<?php
require_once('../model/DatabaseModel.php');
require_once('../model/UserModel.php');

class UIDModel
{
  private static $COLS_FOR_CREATION = "id, type, created_by_user_id, created_on";

  private $id;
  private $typeId;
  private $userId;
  private $createdOn;
  
  private function __construct($row){
    $this->id        = $row['id'];
    $this->typeId    = $row['type'];
    $this->userId    = $row['created_by_user_id'];
    $this->createdOn = $row['created_on'];
  }

  public static function findById($id){
    $id = (int)$id;
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('uids');
    
    $sql = "SELECT ".self::$COLS_FOR_CREATION." FROM $table WHERE id=$id";
    $result =  $database->executeSql($sql);
    if (is_object($result) && $result->num_rows>0){
      return new UIDModel($result->fetch_assoc());     
    }
    else {
      return null;
    }
  }

  /** 
   * Creates a new UID of the given type, trows Exception on errors
   * 
   * @param user UserModel, that wants to create the UID
   * @param typeId integer, the type of UID to create
   * 
   * @return the integer value of the UID
   */
  protected static function createNewUID($user, $typeId){
    if (is_object($user)){
      $database = DatabaseModel::getInstance();
      $table = $database->getTableNameWithPrefix('uids');
      
      $userId = (int)($user->getId());
      $typeId = (int)$typeId;
      $createdOn = date('Y-m-d H:i:s');
      
      $sql = "INSERT INTO $table (type, created_by_user_id, created_on) VALUES ($typeId, $userId, '$createdOn')";
      $result =  $database->executeSql($sql);
      if ((!$database->isResultSuccess($result))){
        print $sql;
        throw new Exception("Failed to insert UID into database");
      }
      else {
        return $database->getLastInsertId();
      }
    }
    else {
      throw new Exception("Failed to create UID");
    }
  }

  public function getTypeId(){
    return $this->typeId;
  }

  function getId(){
    return $this->id;
  }
  
  function getCreatedOn(){
    return $this->createdOn;
  }

  function getUser(){
    return UserModel::findById($this->userId);
  }

  
}

?>