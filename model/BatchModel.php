<?php

require_once('../model/UIDModel.php');
require_once('../model/UIDTypeModel.php');
  
class BatchModel extends UIDModel
{
  public static function createNew($user){
    return parent::createNewUID($user, UIDTypeModel::findTypeByName('Batch'));
  }

  public static function findById($id){
    $object = parent::findById($id);
    if (!is_null($object)){
      if ($object->getTypeId() != UIDTypeModel::findTypeByName('Batch')){
        $object = null;
      }
    }
    return $object;
  }

}

?>
