<?php

require_once('../config.php');
require_once('../utils/PasswordHash.php');
require_once('../model/DatabaseModel.php');
require_once('../model/AccessModel.php');

class UserModel 
{
  const GUEST_NAME = 'guest';
  const ROLE_SEPARATOR = ',';
  
  private static $instance = null;

  private $id;

  ///Associative array of access strings
  private $access;

  ///MEMBER_DATA: Array of strings with Role names
  private $roles;

  ///MEMBER_DATA: string e-mail address
  private $email;

  private $screenName;
  
  private function __construct($id){
    $this->id = $id;
    $this->access = null;
    $this->roles = null;
    $this->email = null;
    $this->screenName = null;
  }

  public static function findById($id){
    $result = new UserModel($id);
    $result->populateData();
    if ($result->email===null){
      //Populate failed, no such user;
      return null;
    }
    else {
      return $result;
    }
  }
  
  /** 
   * Populates the member data from the database
   * 
   * 
   * @return 
   */
  private function populateData(){
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('users');
    
    $id = (int)$this->id;
    $sql = "SELECT email, screen_name, roles FROM $table WHERE id=$id";
    $result = $database->executeSql($sql);
    if (is_object($result) && $result->num_rows==1){
      $row = $result->fetch_assoc();
      $this->email = $row['email'];
      $this->screenName = $row['screen_name'];
      $this->roles = explode(UserModel::ROLE_SEPARATOR, $row['roles']);
    }
  }

  private function populateAccess(){
    if (is_null($this->roles)){
      $this->populateData();
    }
    $this->access = AccessModel::fetchAccessesForRoles($this->roles);
  }

  /** 
   * Return true if the user has access to the given function
   * 
   * @see accessOk
   *
   * @param function string, the named function
   * 
   * @return bool
   */
  public function hasAccess($function){
    if (is_null($this->access)){
      $this->populateAccess();
    }
    if (array_key_exists($function, $this->access)){
      return true;
    }
    else {
      return false;
    }
  }

  /** 
   * Convinience static wrapper for hasAccess.
   * Returns true if the user has access to the given function
   *
   * If you need a UserModel instance for other purposes as well, consider using hasAccess
   *
   * @see hasAccess
   * 
   * @param function string, the named function
   * 
   * @return bool 
   */
  public static function accessOk($function){
    $user = UserModel::getInstance();
    return $user->hasAccess($function);
  }

  /** 
   * Returns an instance of the current user
   * 
   * 
   * @return UserModel
   */
  public static function getInstance(){
    if (is_null(UserModel::$instance)){
      if (UserModel::isLoggedOn()){
        UserModel::$instance = new UserModel($_SESSION['USER_ID']);
      }
      else {
        UserModel::$instance = new UserModel(UserModel::findGuestId());
      }
    }
    return UserModel::$instance;
  }

  /** 
   * Returns the default roles for a new user
   * 
   * 
   * @return string
   */
  private static function getDefaultNewUserRoles(){
    global $CONFIGURATION;
    $result = "";
    if (array_key_exists('DEFAULT_ROLES', $CONFIGURATION)){
      $result = $CONFIGURATION['DEFAULT_ROLES'];
    }
    return $result;
  }
  
  /** 
   * Adds a user to the database, returns a fault string if the addition of the user failed/
   * 
   * @param email string, the users e-mail
   * @param password, the users plain text password
   * 
   * @return null on success or a string on failure
   */
  public static function addUser($email, $screenName, $password){
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('users');
    $hasher = new PasswordHash(12, false);
    
    $email = $database->escape($email);
    $screenName = $database->escape($screenName);
    $password = $hasher->HashPassword($password);
    $roles = $database->escape(UserModel::getDefaultNewUserRoles());
    
    if (strlen($password)!=60){
      return "Internal error: Hashing of the password failed";
    }
    $password = $database->escape($password);

    $sql = "INSERT INTO $table (email, screen_name, password, roles) VALUES ('$email', '$screenName', '$password', '$roles')";
    $result =  $database->executeSql($sql);
    if ($database->isResultSuccess($result)){
      return null;
    }
    else {
      return "User already seems to exist";
    }
  }

  /** 
   * Attempts to login a user.
   * 
   * @param email string, the users e-mail address
   * @param password string, the users plain text password
   * 
   * @return null on success or a fault string on failure.
   */
  public static function login($email, $password){
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('users');
    $isValid = false;

    $email = $database->escape($email);

    $sql = "SELECT * FROM $table WHERE email='$email'";
    $result = $database->executeSql($sql);
    if (is_object($result) && $result->num_rows==1){
      $row = $result->fetch_assoc();
      $hasher = new PasswordHash(12, false);
      $isValid = $hasher->CheckPassword($password, $row['password']);
      if ($isValid === true){
        $_SESSION['USER_ID'] = $row['id'];
      }
      $result->free();
    }

    return $isValid;
  }

  /** 
   * Logs out the user if logged in
   * 
   */
  public static function logout(){
    $_SESSION['USER_ID'] = null;
    session_destroy();
  }
  
  /** 
   * Returns the loged in state of the user
   * 
   * @return true if the user is logged in, false otherwise
   */
  public static function isLoggedOn(){
    $result = false;
    if (array_key_exists('USER_ID', $_SESSION) and $_SESSION['USER_ID']>0){
      $result = true;
    }
    return $result;
  }

  /** 
   * Attempts to find the "guest users" id.
   * Notice this function will die if no guest user is found.
   * 
   * @return int
   */
  public static function findGuestId(){
    $database = DatabaseModel::getInstance();
    $table = $database->getTableNameWithPrefix('users');
    $guestName = UserModel::GUEST_NAME;
    $sql = "SELECT id FROM $table WHERE email='$guestName'";
    $result = $database->executeSql($sql);
    if (is_object($result) && $result->num_rows==1){
      $row = $result->fetch_assoc();
      return $row['id'];
    }
    else {
      die('<h1 class="ConfigurationError">No "'.$guestName.'" user was found. The database is not populated correctly!!!</h1>');
    }
  }

  /** 
   * Rerturns the numerical id of a User
   * 
   * @return int 
   */
  public function getId(){
    return $this->id;
  }

  public function getScreenName(){
    if ($this->screenName===null){
      $this->populateData();      
    }
    return $this->screenName;
  }
  
}

?>
