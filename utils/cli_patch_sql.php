<?php
$file = fopen( 'php://stdin', 'r' );

require_once('config.php');

$database = $CONFIGURATION['DATABASE_DATABASE'];
$prefix = "";
if (array_key_exists('DATABASE_TABLE_PREFIX', $CONFIGURATION)){
  $prefix = $CONFIGURATION['DATABASE_TABLE_PREFIX'];
}


while($line = fgets($file)) {
  $line = preg_replace ('/^CREATE DATABASE IF NOT EXISTS `seritrack`/', "CREATE DATABASE IF NOT EXISTS `$database`", $line);
  $line = preg_replace ('/USE `seritrack`;/', "USE `$database`;", $line);

  $line = preg_replace ('/(CREATE TABLE IF NOT EXISTS|ALTER TABLE|INSERT INTO) `([^`]+)`/', "$1 `$prefix$2`;", $line);
  
  echo $line;
}

fclose($file);
?>