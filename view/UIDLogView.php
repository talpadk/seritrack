<?php

require_once('../view/UserIconView.php');
require_once('../view/TableView.php');
require_once('../view/UIDLogAddView.php');

require_once('../model/LogEntryModel.php');


class UIDLogView
{
  private $uid;
  
  public function __construct($uid){
    $this->uid = $uid;
  }
  
  public function render(){
    $logLines = LogEntryModel::findByUid($this->uid->getId());
    $tableHeader = array('Date'=>'created',
                         'Message'=>'message',
                         'By'=>'user');
    $tableData = array();
    foreach ($logLines as $element){
      $widget = new UserIconView($element->getUser());
      $row = array();
      $row['created'] = $element->getCreated();
      $row['message'] = $element->getMessage();
      $row['user'] = $widget->renderToText();
      $tableData[] = $row;
    }

    print '<div class="UIDLogView">';

    $widget = new TableView($tableHeader, $tableData);
    $widget->render();

    $widget = new UIDLogAddView($this->uid);
    $widget->render();
    
    print '</div>';

  }
}

?>
