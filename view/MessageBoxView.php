<?php

class MessageBox
{
  private $message;
  public function __construct($message){
    $this->message = $message;
  }

  public function render(){
    print '<div class="Message">';
    print $this->message;
    print '</div>';
  }
  
}

?>
