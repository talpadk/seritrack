<?php

class UIDLogAddView
{
  private $uid;
  
  public function __construct($uid){
    $this->uid = $uid;
  }
  
  public function render(){
    print '<div class="UIDLogAddView">';
    print '<form method="post" action="/?group=uid&action=add_log_entry">';
    print '<input type="hidden" name="uid" value="'.$this->uid->getId().'" />';
    print '<input class="Comment" type="text" name="comment" />';
    print '<input type="submit" value="Add" />';
    print '</form>';
    print '</div>';
  }
}

?>
