<?php

class RedirectView
{
  private $url;
  private $delay;
  
  public function __construct($url, $delay=0){
    $this->url = $url;
    $this->delay = $delay;
  }
  
  public function render(){
    header("refresh:$this->delay;url=$this->url" );
  }
}

?>
