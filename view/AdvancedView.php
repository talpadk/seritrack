<?php

class AdvancedView
{
  public function __construct(){
    
  }
  
  public function renderToText(){
    ob_start();
    $this->render();
    $result = ob_get_contents();
    ob_end_clean();
    return $result;
  }
}

?>
