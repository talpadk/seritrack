<?php

require_once('../view/JSView.php');

$code = <<<'EOT'

$(".UserSignupFormPassword").keyup(function() {
  if ($(this).val() != $(this).closest("FORM").find(".UserSignupFormPasswordFirst").val()){
    $(this).parent().find(".Warning").show();
    $(this).closest("FORM").find(".UserSignupFormSubmit").prop("disabled", true);
  }
  else {
    $(this).parent().find(".Warning").hide();
    $(this).closest("FORM").find(".UserSignupFormSubmit").prop("disabled", false);
  }
});

$(".UserSignupFormPasswordFirst").keyup(function() {
  if ($(this).val() != $(this).closest("FORM").find(".UserSignupFormPassword").val()){
    $(this).closest("FORM").find(".Warning").show();
    $(this).closest("FORM").find(".UserSignupFormSubmit").prop("disabled", true);
  }
  else {
    $(this).closest("FORM").find(".Warning").hide();
    $(this).closest("FORM").find(".UserSignupFormSubmit").prop("disabled", false);
  }
});
EOT;

JSView::addJQuery('UserSignupFormView', $code);

class UserSignupFormView 
{
	public function __construct() {	

	}

	public function render(){
		print '<div class="UserSignupFormView">';
		print '<form method="post" action="/?group=user&action=do_signup"><table>';
		print '<tr><th>Screen name</th><td><input type="text" name="screen_name" /></td></tr>';
		print '<tr><th>E-mail</th><td><input type="text" name="email" /></td></tr>';
		print '<tr><th>Password</th><td><input class="UserSignupFormPasswordFirst" type="password" name="password" /></td></tr>';
		print '<tr><th>Repeat password</th><td><input class="UserSignupFormPassword" type="password" name="password2" /><span class="Warning">'."Passwords don't match".'</span></td></tr>';
    print '<tr><th> </th><td><input class="UserSignupFormSubmit" type="submit" value="Signup"/></td></tr>';
		print '</table></form>';
		print '</div>';
	}
}

?>
