<?php

require_once('../model/UserModel.php');

class CreateSerialNumberView
{
  public function __construct(){
    
  }
  
  public function render(){
    if (!UserModel::accessOk('SN Create')){ return ; }
    
    print '<div class="CreateSerialNumberView">';
    print '<form method="post" action="/?group=serial_number&action=create">';
    print '<input type="submit" value="Create Serial Number" />';
    print '</form>';
    print '</div>';
  }
}

?>
