<?php

class JSView 
{
	static private $jQuery = array();
	
	private function __construct() {	

	}

	static public function addJQuery($name, $code){
		JSView::$jQuery[$name]=$code;
	}

	static public function render(){
		print '<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>'."\n";
		
		print '<script type="text/javascript">'."\n";
		print '$(document).ready(function(){';

		foreach (JSView::$jQuery as $key => $value){
			print $value;
      print "\n";
		}
		print '});';
    print '</script>';

	}


}

?>
