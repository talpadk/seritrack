<?php

require_once('../view/CreateSerialNumberView.php');
require_once('../view/CreateBatchView.php');
require_once('../view/UIDSearcherView.php');
  
class FrontPageView 
{
	private $action;
	
	public function __construct() {	
	}

	public function render(){
    print "<h1>Front page</h1>";
    print "Create new UIDs ";
    $widget = new CreateSerialNumberView();
    $widget->render();
    $widget = new CreateBatchView();
    $widget->render();    
    Print "Search for UID ";
    $widget = new UIDSearchView();
    $widget->render();
	}
}

?>
