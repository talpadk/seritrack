<?php

require_once('../model/UserModel.php');
require_once('../view/AdvancedView.php');


class UserIconView extends AdvancedView
{
  private $user;

  /** 
   * A view that displays a short user "icon"
   * 
   * @param user UserModel the user to display
   */
  public function __construct($user){
    parent::__construct();
    $this->user = $user;
  }
  
  public function render(){
    print '<span class="UserIconView">';
    if ($this->user===null){
      print "No Body";
    }
    else {
      print $this->user->getScreenName();
    }
    print '</span>';
  }
}

?>
