<?php

require_once('../model/UserModel.php');

class CreateBatchView
{
  public function __construct(){
    
  }
  
  public function render(){
    if (!UserModel::accessOk('Batch Create')){ return ; }
    
    print '<div class="CreateBatchView">';
    print '<form method="post" action="/?group=batch&action=create">';
    print '<input type="submit" value="Create Batch Number" />';
    print '</form>';
    print '</div>';
  }
}

?>