<?php

require_once('../model/FileLocationsModel.php');
require_once('../model/FileThumbnailModel.php');

class UIDFilesView
{
  public $uid;
  
  public function __construct($uid){
    $this->uid = $uid;
  }
  
  public function render(){
    global $CONFIGURATION;
    
    print '<div class="UIDFilesView">';
    print '<h2>Files</h2>';
    $path = FileLocationsModel::getUIDPath($this->uid);
    $files = array();
    if (file_exists($path)){
      $files = scandir($path);
    }
    print '<div>';
    foreach ($files as $file){
      $filename = "$path/$file";
      if (is_file($filename)){
        print '<div class="File">';
        print FileThumbnailModel::getThumbailAsHtmlImage($filename);
        print '<br /><a href="/?group=file&action=download&file='.urlencode($filename).'">'.$file.'</a></div>';
        print "\n";
      }
    }
    print '</div>';

    $maxSize = (int)$CONFIGURATION['UPLOAD_MAX_SIZE'];
    if ($maxSize>=1024*1024){
      $maxSize /= 1024*1024;
      $maxSize = sprintf("%.0f MB", $maxSize);
    }
    elseif ($maxSize>=1024){
      $maxSize /= 1024;
      $maxSize = sprintf("%.0f kB", $maxSize);
    }
    else {
      $maxSize = "$maxSize Bytes";
    }
    
    print '<div class="FileUpload">';
    print '<h3>File Upload</h3>';
    print '<form enctype="multipart/form-data" action="/?group=file&action=upload" method="POST">';
    print '<input class="FileSelector" name="upload_file" type="file" size="20"/>';
    print '<input type="hidden" name="uid" value="'.Base32Model::fromInt($this->uid->getId()).'" />';
    print '<input type="submit" value="Upload" />';
    print " <span class=\"Note\">($maxSize limit)<span>";
    print '</form>';
  

    print '</div>';
    print '</div>';
  }
}

?>
