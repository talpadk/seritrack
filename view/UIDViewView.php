<?php

require_once('../model/Base32Model.php');
require_once('../model/UIDTypeModel.php');
require_once('../model/UIDLocationModel.php');

require_once('../view/UserIconView.php');
require_once('../view/UIDLogView.php');
require_once('../view/UIDFilesView.php');
require_once('../view/UIDLinkView.php');

class UIDViewView
{
  private UIDModel $uid;
  
  public function __construct(UIDModel $uid){
    $this->uid = $uid;
  }
  
  public function render(){
    $uidString = Base32Model::fromInt($this->uid->getId());
    $typeId = $this->uid->getTypeId();
    $type = UIDTypeModel::findTypeNameById($typeId);
    $widget = new UserIconView($this->uid->getUser());
    print '<h1>';
    print "$type: $uidString";
    print '</h1>';
    print '<span class="Note">Created on: '.$this->uid->getCreatedOn();
    print ' by ';
    $widget->render();
    print '</span>';
    
    $widget = new UIDLogView($this->uid);
    $widget->render();

    if ($typeId == UIDTypeModel::SERIAL_NUMBER ||
        $typeId == UIDTypeModel::BATCH ){
      $location = UIDLocationModel::getLocationForUID($this->uid);
      if (!is_null($location)){
        $widget = new UIDLinkView($location);
        print "Current location is ".$widget->renderToText()."<br/>";
      }
      print '<a href="/?group=uid&action=prepare_move&uid='.$uidString.'">Manage location and content</a>';
    }
    
    $widget = new UIDFilesView($this->uid);
    $widget->render();

  }
}

?>
