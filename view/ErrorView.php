<?php

class ErrorView {
	private $message;
	
	public function __construct($message){
		$this->message = $message;
	}
	
	public function render(){
		print '<div class="ErrorView">';
		print $this->message;
		print '</div>';
	}
}

?>
