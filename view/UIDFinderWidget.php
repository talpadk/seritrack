<?php

class UIDFinderWidget {

  private $fieldName;
  private $defaultValue;
  private $className;
  private $typeOfUID;
  
  public function __construct($fieldName, $defaultValue=null, $className=null, $typeOfUID=null){
    $this->fieldName = $fieldName;
    $this->defaultValue = $defaultValue;
    $this->className = $className;
    $this->typeOfUID = $typeOfUID;
  }

  public function render(){
    print '<input type="text" name="'.$this->fieldName.'"';
    if (!is_null($this->className)){
      print ' class="'.$this->className.'"';
    }
    if (!is_null($this->defaultValue)){
      print ' value="'.$this->defaultValue.'"';
    }
    print ' />';
    
  }
  
}

?>
