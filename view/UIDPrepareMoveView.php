<?php

require_once('../model/Base32Model.php');
require_once('../model/UIDTypeModel.php');
require_once('../model/UIDLocationModel.php');

require_once('../view/UserIconView.php');
require_once('../view/UIDLogView.php');
require_once('../view/UIDFilesView.php');
require_once('../view/UIDLinkView.php');
require_once('../view/UIDFinderWidget.php');

class UIDPrepareMoveView
{
  private $uid;
  
  public function __construct($uid){
    $this->uid = $uid;
  }
  
  public function render(){
    $uidString = Base32Model::fromInt($this->uid->getId());
    $typeId = $this->uid->getTypeId();
    $type = UIDTypeModel::findTypeNameById($typeId);
    $widget = new UserIconView($this->uid->getUser());
    print '<h1>';
    print "$type: $uidString";
    print '</h1>';
    print '<span class="Note">Created on: '.$this->uid->getCreatedOn();
    print ' by ';
    $widget->render();
    print '</span>';

    $location = UIDLocationModel::getLocationForUID($this->uid);
    $locationIdString = null;
    if (is_null($location)){
      print '<h2>Currently has no location</h2>';
    }
    else {
      $locationIdString = Base32Model::fromInt($location->getId());
      print '<h2>Location: ';
      $widget = new UIDLinkView($location);
      $widget->render();
      print '</h2>';
    }
    print '<form action="/?group=location&action=move" method="post">';
    print '<input type="hidden" name="obj_to_move_uid_string" value="'.$uidString.'" />';
    print 'Set location to: ';
    $widget = new UIDFinderWidget('location_uid_string', $locationIdString);
    $widget->render();
    print '<input type="submit" value="Set" />';
    print '</form>';

    
    if ($typeId == UIDTypeModel::SERIAL_NUMBER){
      print '<h2>Content</h2>';

      print '<form action="/?group=location&action=move" method="post">';
      print '<input type="hidden" name="location_uid_string" value="'.$uidString.'" />';
      print 'Add content: ';
      $widget = new UIDFinderWidget('obj_to_move_uid_string');
      $widget->render();
      print '<input type="submit" value="Add" />';
      print '</form>';
      
      $content = UIDLocationModel::getContentForUID($this->uid);
      if (is_null($content)){
        print 'There are currently no content';
      }
      else {
        print '<div class="Content">';
        foreach ($content as $item){
          print '<div>';
          $widget = new UIDLinkView($item);
          $widget->render();
          print '</div>';
        }
        print '</div>';
      }
    }
    
  }
}

?>
