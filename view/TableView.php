<?php

class TableView
{
  private $header;
  private $data;
  
  public function __construct($tableHeader, $tableData){
    $this->header = $tableHeader;
    $this->data = $tableData;
  }

  public function render(){
    print '<table class="TableView">';
    print '<tr>';
    foreach ($this->header as $key=>$value){
      print '<th>'.$key.'</th>';
    }
    print '</tr>';
    foreach ($this->data as $row){
      print '<tr>';
      foreach ($this->header as $key=>$value){
        print '<td>'.$row[$value].'</td>';
      }
      print '</tr>';
    }
    print '</table>';

  }
  
}

?>
