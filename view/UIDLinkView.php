<?php

require_once('../view/AdvancedView.php');

class UIDLinkView extends AdvancedView
{
  private $uid;
  private $contents;

  public function __construct($uid, $linkContents=null){
    parent::__construct();
    $this->uid = $uid;
    $this->contents = $linkContents;
  }

  public function render(){
    $contents = $this->contents;
    if (is_null($contents)){
      if (is_object($this->uid)){
        $contents = Base32Model::fromInt($this->uid->getId());
      }
      else {
        $contents = "?Unknown?";
      }
    }

    $linkBegin = "";
    $linkEnd   = "";
    if (is_object($this->uid)){
      $base32 = Base32Model::fromInt($this->uid->getId());
      $linkBegin = '<a href="/?group=uid&action=view&uid='.$base32.'">';
      $linkEnd   = '</a>';
    }

    print $linkBegin.$contents.$linkEnd;
  }
}

?>
